// Descripción: suma una lista de números.
//      Imprime el resultado en el terminal
//
// Objetivo:
//      * Introducción a la programación con lenguajes compilados (C).
//      * Enlazado dinámico de funciones de bibliotecas de programación.
//      * Introducción a la depuración de programas en lenguaje C.


// Incluímos funciones de la biblioteca de entrada/salida estándar (printf)
#include <stdio.h>

int main(void)
{
    long data[] = {1, 5, 2, 18};    // lista de númeors
    long count = 4;                 // contador de números restantes
    long sum = 0;                   // acumulador del resultado
    long i = 0;                     // índice de la lista de números
    while (count > 0) {
        sum = sum + x[i];
        i++;                // i = i + 1
        count--;            // count = count - 1
    }
    printf("sum = %ld\n", sum);
}

/*
    EJERCICIO

    1. Compila y ejecuta el programa. Comprueba que el resultado de la suma
       es correcto.

        $ gcc -o add_list add_list.c
        $ ./add_list

    2. Ahora vamos a decirle al compilador que ejecute sólo la fase de
       compilación y genere el código ensamblador, con la opción '-S':

        $ gcc -S -o add_list.s add_list.c

       Abre el archivo add_list.s y observa el código generado por el
       compilador. Compara el código con el escrito para el ejemplo 3.
       Localiza el código los siguientes elementos:

       * Las variables del programa: se almacenan en direcciones relativas
         al puntero base %rbp, por ejemplo, '-56(%rbp)'.

       * La instrucción que suma los números en 'sum', la que incrementa el
         índice 'i' y la que decrementa el contador 'count'.

       * La instrucción que llama a la función de biblioteca 'printf'.

       * La instrucción que finaliza la ejecución de la función 'main'.

    3. (Avanzado) Desensambla el programa ejecutable con el siguiente comando:

        $ objdump -d add_list | less
        ...

       Observa que el código final del programa contiene muchas secciones
       además de la sección 'main', que es la que corresponde al código C,
       vista en el apartado anterior. Estas secciones preparan el código para
       que pueda ser ejecutado por el sistema operativo y para que pueda
       utilizar las funciones de la biblioteca estándar de C.

       Observa que el código de la función 'printf' no aparece en el programa
       ejecutable. El código de esta función será cargado por el enlazador
       dinámico automáticamente cuando el programa sea cargado en memoria para
       su ejecución. Si varios programas usan la función 'printf', lo cual
       es muy probable, el enlazador sólo cargará en memoria una copia del
       mismo para optimizar el uso de la memoria.
*/
