# Apuntes de ensamblador Intel x86 en GNU/Linux

Una introducción rápida a la programación en ensamblador de la arquitectura
x86 (procesadores Intel/AMD de 32 y 64 bits) en sistemas GNU/Linux, basada en
ejemplos.

Los ejemplos principales son para código de 64 bits (arquitectura x86-64).

Los ejemplos usan código al estilo del esnamblador de GNU (GAS) y han sido
probados sobre la plataforma GNU/Linux. Se indican los comando para probar
los ejemplos en este plataforma. La mayoría de los ejemplos deberían poder
ensamblarse en cualquier plataforma con acceso al ensamblador y biblioteca
de C de GNU, quizá con cambios en las opciones de los comandos.

## Audiencia

Lectores con alguna experiencia en programación que quieran tener una idea
básica del funcionamiento del software a nivel de código ensamblador.

## Bibliografía

* [Programming from the Ground UP](https://savannah.nongnu.org/projects/pgubook/) -- Libro para aprender a programar en ensamblador x86-32. Da una visión competa de la progración y su relación con el funcionamiento del computador.
* [x86 Assembly](https://en.wikibooks.org/wiki/X86_Assembly) -- Manual de referencia de ensamblador x86 (32 y 64 bits).
* [System V ABI](https://wiki.osdev.org/System_V_ABI) -- Convenios de llamada
  a funciones para arquitecturas Intel de 32 y 64 bits.

## Registros

Registros de propósito general. Pueden emplearse en cualquier operación
aritmética, pero tienen asignadas funciones específicas.

64 bit | 32 bit | 16 bit | Nombre             | Propósito
-------|--------|--------|--------------------|------------------------------
RAX    | EAX    | AX     | Accumulator reg.   | Operaciones aritméticas
RCX    | ECX    | CX     | Counter reg.       | Contador
RDX    | EDX    | DX     | Data reg.          | Operaciones aritméticas y E/S
RBX    | EBX    | BX     | Base reg.          | Puntero
RSP    | ESP    | SP     | Stack Pointer      | Puntero de pila
RBP    | EBP    | BP     | Stack Base Pointer | Base de la pila
RSI    | ESI    | SI     | Source Index       | Puntero a fuente de datos
RDI    | EDI    | DI     | Destination Index  | Puntero a destino de datos

Registros adicionales en mode de 64 bits: **R8** a **R15**.

Registros especiales:

* EFLAGS: registro de estado (banderas). Algunas banderas:
  - 0, CF, Carry Flag. Acarreo en la última operación
  - 6, ZF, Zero Flag. Último resultado igual a cero.
  - 7, SF, Sign Flag. Último resultado es negativo.
  - 9, IF, Interrupt Flag. Las interrupciones están habilitadas.
  - 11, OF, Overflow Flag. Desbordamiento en la última operación con signo.

## Herramientas

Ensamblar con códigos de depuración (`-g`):

    as -g prog.s -o prog.o

(Modo 32 bits):

    as -g --32 prog.s -o prog.o

Enlazar código:

    ld -o prog prog.o

(Modo 32 bits):

    ld -m elf_i386 -o exit exit.o

Enlazar código con llamadas a la biblioteca estándar de C:

    ld -dynamic-linker /lib64/ld-linux-x86-64.so.2 -lc -o printf printf.o

(Modo 32 bits):

    ld -m elf_i386 -dynamic-linker /lib/ld-linux.so.2 -lc -o printf printf.o

Depurar con *gdb*, con archivo de configuración inicial:

    gdb -x gdb.conf prog

Depurar con *ddd*:

    ddd prog &

Desensamblar:

    objdump -d prog

Compilar, ensamblar y enlazar un programa en lenguaje *C* (añadir
*-g* para códigos de depuración):

    gcc -o prog prog.c

Sólo compilar un programa en lenguaje *C*. Genera código
ensamblador (añadir *-g* para códigos de depuración):

    gcc -S -o prog.s prog.c

## Llamadas al sistema del kernel Linux

### Modo 64 bits

Las llamadas al sistema se ejecutan mediante la instrucción **syscall**.

* Código de la llamada: %rax
* Parámetros: rdi, rsi, rdx, r10, r8, r9.
* Valor de retorno: rax

Ver archivo **/usr/include/asm/unistd_64.h** para códigos de llamadas.

Ejemplos:

Número  | Llamada
-------:|:-------
  0     | read
  1     | write
  2     | open
  3     | close
  60    | exit
  85    | creat

### Modo 32 bits

Las llamadas al sistema se ejecutan mediante la instrucción **int $0x80**.

* Código de la llamada: %eax
* Parámetros: ebx, ecx, edx, esi, edi, ebp.
* Valor de retorno: eax

Ver archivo **/usr/include/asm/unistd_32.h** para códigos de llamadas.

Ejemplos:

Número  | Llamada
-------:|--------
  1     | exit
  3     | read
  4     | write
  5     | open
  6     | close
  8     | creat

## Paso de parámetros a funciones

### Modo 64 bits

Los 6 primeros parámetros, si son de tipo **entero**, se pasan por orden
en los siguientes registros:

|   1 |   2 |   3 |   4 |  5 |  6 |
------|-----|-----|-----|----|----|
| rdi | rsi | rdx | rcx | r8 | r9 |

Los parámetros enteros incluyen cualquier valor entero de hasta 8 bytes y
los punteros. El resto de parámetros se pasan en la pila en orden inverso.
La función llamante es responsable de restaurar la pila tras la llamada a
la función.

Para parámetros enteros, rax=0.

El valor de retorno se devuelve en rax si es de 64 bits o menor, y en rdx:rax
si es hasta 128 bits.

rbx, rbp y r12-r15 deben ser salvados por la función llamada.
El resto por la función llamante.

### Modo 32 bits

Los parámetros se pasan a través de la pila en orden inverso. La función
llamante es responsable de restaurar la pila tras la llamada a la función.

## Instrucciones

Las instrucciones operan sobre datos almacenados en registros o en posiciones
de la memoria.

### Formato de instrucciones (sintáxis AT&T)

En general el formato es:

    <nemónico> <origen>, <destino>

* <nemónico>: nombre simbólico de la instrucción.
* <origen>: operando 1 u origen.
* <destino>: operando 2 y/o destino del resultado.
* <origen> o <destino> son opcionales según la instrucción.
* Algunas instrucciones tienen más de dos operandos.

Los nemónicos llevan un sufijo en función del tamaño de datos que manejan:
* b (byte): 8 bits
* w (word): 16 bits
* l (long): 32 bits
* q (quad): 64 bits

Ejemplos: *movq*, *addl*, etc.

### Tipos de operandos

* Números "inmediatos": constantes numéricas. Precedidas por '$'. Prefijos de
  formato: hexadecimal (**0x**), octal (**0**), binario (**0b**). Ejemplos:
  - *$17*: decimal 17.
  - *$0xa7*: hexadecimal a7 (decimal 167).
  - *$0b1001*: binario 1001 (decimal 9).

* Registros: precedidos por '%'. Ejemplos: *%rax*, *%ecx*, etc.

* Direcciones de memoria: pueden indicar una dirección numérica fija, una
  dirección contenida en un registro o una combinación de ambas.

### Formado de direcciones de memoria

El formato general es:

    <desplazamiento>(<rbase>, <ríndice>, <factor>)

Cálculo de la dirección:

    dirección = desplazamiento + rbase + ríndice * factor

Todas las componentes son opcionales. Ejemplos:

* *my_data*: dirección dada por una etiqueta.
* *(%rbx)*: dirección contenida en %rbx.
* *16(%rbx)*: dirección = %rbx + 16.
* *(%rbx%,%rcx,8)*: dirección = %rbx + %rcx * 8.
* *my_data(,%ecx,4)*: dirección = my_data + %ecx * 4.

### Tipos de instrucciones

* Transferencia de datos: *mov*, *push*, *pop*, *in*, *out*, etc.
* Salto incondicional: *jmp*
* Comparación y salto condicional: *cmp*, *jxx*, etc.
* Subrutinas: *call*, *ret*.
* Aritméticas: *add*, *sub*, *mul*, *imul*, *div*, *idiv*, *neg*,
  *inc*, *dec*, etc.
* Lógicas: *and*, *or*, *xor* y *not*.
* Desplazamiento de bits: *shr*, *shl*, *sar*, *sal*, etc.

## Descripción de los ejemplos

Los ejemplos que se incluyen muestran diversos aspectos básicos de la
programación en ensamblador y del funcionamiento de los programas
compilados/ensamblados. Los archivos fuente de los ejemplos incluyen
explicaciones y ejercicios propuestos.

Para una itroducción de tipo tutorial se aconseja revisar los ejemplos en el
orden de la numeración de las carpetas.

La carpeta 'extra' incluye ejemplos adicionales.

* 1. *add*: suma de dos números en ensamblador (ejemplo básico).

* 2. *add_c*: suma de dos números en lenguaje C. Comparación con la versión
     en ensamblador.

* 3. *add_list*: suma de una lista de números en ensamblador.

* 4. *write*: llamadas al sistema desde ensamblador.

* 5. *add_list*: suma de una lista de números en C. Comparación con la versión
     en ensamblador. Introducción a las llamadas a funciones y enlazado
     dinámico de bibliotecas de funciones.

* 6. *printf*: llamadas a funciones de la biblioteca estándar de C desde
     ensamblador. Paso de parámetros a funciones. Enlazado dinámico de
     código ensamblador.

* 7. *count*: llamada a printf con parámetros numéricos. Salvar y recuperar
     el estado de los registros (push y pop).

## Autores

* **Jorge Juan-Chico**

## Licencia

This is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version. See
<http://www.gnu.org/licenses/>.

## Agradecimientos

* Mis estudiantes de la Licenciatura en Estadística de la [Facultad de
  Matemáticas](https://www.matematicas.us.es/) que han motivado la mayoría
  de estos ejemplos.
