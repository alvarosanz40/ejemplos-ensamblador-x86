# Descripción: escribe un mensaje en el terminal usando la
#       función 'printf' de la biblioteca etándar de C.
#       Código de 32 bits x86-32.
#
# Uso: construir en x86-64:
#   $ as --32 -o printf.o printf.s
#   $ ld -m elf_i386 -dynamic-linker /lib/ld-linux.so.2 -lc -o printf printf.o
#

.section .data
message:                        # printf espera cadenas terminadas en
    .ascii "Hola mundo!\n\0"    # el carácter nulo (\0)

.section .text

.globl _start
_start:
    # En la arquitectura x86-32 los parámetros a las funciones se
    # pasan a través de la pila en orden inverso. En este caso sólo
    # hay un parámetro: la cadena de formato de printf.
    pushl $message
    call printf

    pushl $0
    call exit
