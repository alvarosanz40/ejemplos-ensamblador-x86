# Suma dos números
#
#   $ as -g --32 -o add.o add.s
#   $ ld -m elf_i386 -o add add.o
#
#   %eax: syscall code (4 for 'write')
#   %ebx: file descriptor (1 for 'stdout')
#   %ecx: pointer to the message
#   %edx: number of bytes to write

.section .data          # datos del programa
                        # no hay datos en este programa

.section .text          # código del programa

.globl _start           # '_start' es un símbolo que indica dónde comienza
_start:                 # el programa. Debe ser un símbolo 'global'

    movl $10, %eax      # eax = 10
    movl $14, %ecx      # ecx = 14
    addl %eax, %ecx     # ecx = ecx + eax

    movl $1, %eax       # 'int $0x80' activa una interrupción que es el
    movl $0, %ebx       # canal de comunicación con el kernel de s.o.
                        # eax indica la función requerida: 1 -> exit
                        # ebx es el valor de retorno del programa
    int $0x80           # 0 -> todo correcto (ver con 'echo $?')

# EJERCICIO
#
# Cambia el código para devolver como valor de retorno el resultado de la
# suma.
