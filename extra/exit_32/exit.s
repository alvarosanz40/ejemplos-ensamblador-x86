# How to exit a program in i386 assembly

.section .data      # program data starts here
                    # no date this time though

.section .text      # program code starts here

.globl _start       # '_start' is a symbol that identifies where our
_start:             # program starts. It should be global

    movl $1, %eax   # 1 is 'exit' syscall
    movl $7, %ebx   # return value is 0 means 'everything ok'
                    # change at will and check with:
                    # echo $?
    int $0x80       # wakes up the kernel to handle the syscall
