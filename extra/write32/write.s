# Write a message to the standard output using the 'write' syscall
# This code used 32bit style. Compile and link with:
#
#   $ as --32 -o write.o write.s
#   $ ld -m elf_i386 -o write write.o
#
#   %eax: syscall code (4 for 'write')
#   %ebx: file descriptor (1 for 'stdout')
#   %ecx: pointer to the message
#   %edx: number of bytes to write

.section .data
message:
    .ascii "Hello world!\n"
message_end:

# Calculate message length
.equ message_len, message_end - message

.section .text

.globl _start
_start:
    # setup 'write' system call
    movl $4, %eax               # 4 is 'write' system call code
    movl $1, %ebx               # 1 is 'stdout' file descriptor
    movl $message, %ecx         # data pointer
    movl $message_len, %edx     # data lenght
    int $0x80                   # make the syscall

    # setup 'exit' system call
    movl $1, %eax               # 1 is 'exit' syscall
    movl $0, %ebx               # return value is 0 (everything ok)
    int $0x80                   # make the syscall
