# Descripción: escribe un mensaje en el terminal usando la función
#       'printf' de la biblioteca de C. Cógico ensamblable con gcc
#
# Construir:
#   $ as -o printf.o printf.s
#   $ ld -dynamic-linker /lib64/ld-linux-x86-64.so.2 -lc -o printf printf.o
#
# O bien:
#   $ gcc -nostartfiles -lc -o printf printf.s
#
#   gcc necesita 'lea' en vez de 'mov' y 'call ...@plt'
#
# Lista de parámetros en llamadas a funciones en x86-64:
#       rdi, rsi, rdx, rcx, r8, r9
# Retorno:
#       rax, rdx

.section .data
message:                        # printf necesita que la cadena termine
    .ascii "Hola mundo!\n\0"    # en el carácter nulo (\0)

.section .text

.globl _start
_start:
    lea message(%rip), %rdi
    movq $0, %rax
    call printf@plt

    movq $0, %rdi
    call exit@plt
