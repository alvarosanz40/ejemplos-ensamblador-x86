# Descripción: imprime una cuenta de números usando la función 'printf' de
#       la biblioteca de C.
#
# Objetivo:
#       * Mostrar la forma de llamar a funciones de la biblioteca estándar
#         de C desde código ensamblador.
#       * Aprender a llamar a 'printf' desde ensamblador para imprimir
#         mensajes y números.
#       * Uso de 'push' y 'pop' para salvar y recuperar el valor de registros.
#
# Notas:
# La lista de parámetros en llamadas a funciones en x86-64 se pasan a la
# función en los siguientes registros en el orden mostrado:
#       rdi, rsi, rdx, rcx, r8, r9
# El valor retornado por la función se devuelve en los registros:
#       rax, rdx
#
# Uso de registros:
#       %eax: debe ponerse a cero antes de llamar a printf
#       %rdi: dirección de la cadena de formato
#       %rsi: valor a imprimir
#
#       rbx, rbp y r12-r15 son 'callee-saved' por lo que printf no cambiará su
#       valor tras la llamada. El resto de registros pueden ser modificados
#       por la función.

.equ COUNT, 20                  # valor máximo de la cuenta

.section .data
message:                        # mensaje inicial
    .ascii "Contando...\n\0"    # printf necesita que la cadena termine
                                # en el carácter nulo (\0)
format:                         # cadena de formato para imprimir un número
    .ascii "Valor: %lu\n\0"     # %lu indica un número entero de 64 bits sin
                                # signo (long unsigned)
.section .text

.globl _start
_start:
    # Imprimimos el mensaje inicial
    movq $message, %rdi         # parámetro 1: dirección de la cadena de texto
    movl $0, %eax               # %al=0 si no hay parámetros en p. flotante
    call printf                 # llamada a la función 'printf'

    movq $0, %rsi               # iniciamos el contador %rsi y la cadena de
    movq $format, %rdi          # formato para printf

loop:
    cmp $COUNT, %rsi            # finalizamos si hemos completado la cuenta
    je finish

    movl $0, %eax               # preparamos llamada a printf
    push %rdi                   # salvalos registros en la pila
    push %rsi
    call printf                 # llamada a printf
    pop %rsi                    # recuperamos los registros de la pila
    pop %rdi                    # (en orden inverso)

    incq %rsi                   # incrementamos el valor de cuenta y repetimos
    jmp loop

finish:
    # Para terminar el programa, en vez de usar la llamada al sistema 'exit',
    # usaremos la función 'exit' de C que hace la misma tarea.
    movq $0, %rdi               # parámetro: valor de retorno (0)
    call exit                   # llamada a la función 'exit'

/*
    EJERCICIO

    1. Ensambla y enlaza el programa con los siguientes comandos:

        $ as -o count.o count.s
        $ ld -dynamic-linker /lib64/ld-linux-x86-64.so.2 -lc -o count count.o

       El programa generado contendrá el código máquina correspondiente y la
       información necesaria para cargar dinámicamente el código de la
       biblioteca de C cuando el programa sea ejecutado.

       Explicación de las opciones nuevas en el enlazador:

       * -dynamic-linker /lib64/ld-linux-x86-64.so.2: indica el programa
         para el enlazado dinámico que se debe usar al ejecutar el programa.

       * -lc: indica que se incluya la biblioteca dinámica de C en el enlazado
         dinámico, ya que esta biblioteca es la que incluye las funciones
         'printf' y 'exit'.

    2. Ejecuta el programa y observa si imprime el mensaje esperado:

        $ ./count

    3. Prueba a cambiar el número de valores a contar en el programa, ensambla
       y ejecuta de nuevo.
 */
