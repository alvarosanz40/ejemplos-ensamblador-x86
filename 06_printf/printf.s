# Descripción: escribe un mensaje en el terminal usando la función
#       'printf' de la biblioteca de C
#
# Objetivo:
#       * Mostrar la forma de llamar a funciones de la biblioteca estándar
#         de C desde código ensamblador.
#       * Mostrar el convenio de paso de parámetros a funciones.
#       * Comprender el proceso de enlazado dinámico de programas.
#
# Notas:
# La lista de parámetros en llamadas a funciones en x86-64 se pasan a la
# función en los siguientes registros en el orden mostrado:
#       rdi, rsi, rdx, rcx, r8, r9
# El valor retornado por la función se devuelve en los registros:
#       rax, rdx

.section .data
message:                        # printf necesita que la cadena termine
    .ascii "Hola mundo!\n\0"    # en el carácter nulo (\0)

.section .text

.globl _start
_start:
    # Preparamos la llamada a la función 'printf' de la biblioteca de C
    # Sólo usaremos un parámetro: la dirección del texto a imprimir
    movq $message, %rdi         # parámetro 1: dirección de la cadena de texto
    movq $0, %rax               # %al=0 si no hay parámetros en p. flotante
    call printf                 # llamada a la función 'printf'

    # Para terminar el programa, en vez de usar la llamada al sistema 'exit',
    # usaremos la función 'exit' de C que hace la misma tarea.
    movq $0, %rdi               # parámetro: valor de retorno (0)
    call exit                   # llamada a la función 'exit'

/*
    EJERCICIO

    1. Ensambla y enlaza el programa con los siguientes comandos:

        $ as -o printf.o printf.s
        $ ld -dynamic-linker /lib64/ld-linux-x86-64.so.2 -lc -o printf printf.o

       El programa generado contendrá el código máquina correspondiente y la
       información necesaria para cargar dinámicamente el código de la
       biblioteca de C cuando el programa sea ejecutado.

       Explicación de las opciones nuevas en el enlazador:

       * -dynamic-linker /lib64/ld-linux-x86-64.so.2: indica el programa
         para el enlazado dinámico que se debe usar al ejecutar el programa.

       * -lc: indica que se incluya la biblioteca dinámica de C en el enlazado
         dinámico, ya que esta biblioteca es la que incluye las funciones
         'printf' y 'exit'.

    2. Ejecuta el programa y observa si imprime el mensaje esperado:

        $ ./printf

    3. Es posible obtener las bibliotecas dinámicas que usa un programa
       con el comando 'ldd'. Prueba el comando con nuestro programa e
       intenta interpretar el resultado.

        $ ldd ./printf

       Prueba el comando 'ldd' con otros archivos ejecutables del sistema.
       Verás el uso extensivo que se hace del enlazado dinámico de bibliotecas
       de programación. Por ejemplo:

        $ ldd /bin/mkdir
        ...

        $ ldd /usr/bin/evince
        ...
 */
